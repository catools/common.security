package org.catools.common.concurrent.tests;

import org.catools.common.extensions.verify.CVerificationInfo;
import org.catools.common.extensions.verify.CVerificationQueue;
import org.catools.common.extensions.verify.CVerify;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CBaseUnitTest implements CVerificationQueue<CVerify> {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    protected CVerify verify = new CVerify(logger);

    protected static Object[] getParams() {
        StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[2];
        return new String[]{
                stackTrace.getClassName()
                        .replace("org.catools.common.tests.verify.noretry.interfaces", "")
                        .replace("org.catools.common.tests.wait.noretry.interfaces", ""), stackTrace.getMethodName()
        };
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public CVerify queue(CVerificationInfo verificationInfo) {
        return verify.queue(verificationInfo);
    }
}